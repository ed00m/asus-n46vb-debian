#!/bin/sh -x
#set -v
#set -e

#||||||||||||||||||||||||||||||||\_________________________
# Boot Modules
#||||||||||||||||||||||||||||||||/´´´´´´´´´´´´´´´´´´´´´´´´´

# Modules
# Sound
#   woofer
# Keyboard



if (grep -i debian /proc/version > /dev/null);then
    if [ -f /etc/rc.local ];then
        target=/etc/rc.local
    else
        echo "File target Not found"
        exit
    fi
    SO="Debian Linux"
elif (grep -i suse /proc/version  > /dev/null);then
    if [ -f /etc/rc.d/boot.local ];then
        target=/etc/rc.d/boot.local
    else
        echo "File target Not found"
        exit
    fi
    SO="SUSE Linux"
fi

dirTMP=/tmp
fileTMP=${dirTMP}/$$_file
user=$(whoami)
kernel=$(uname -r)

echo "SO Detectado: "${SO}
echo "Kernel: "${kernel}

if [ ! "${user}" = "root" ];then
    echo "[] No permissions"
else
    
    if [ -w ${target} ];then
        
        cat ${target}|sed -e "/exit 0/d" > ${fileTMP}_rclocal
        # if exists, Clearing values
        sed -i -e "/echo 0x16 0x99130112/d" ${fileTMP}_rclocal
        sed -i -e "/sys\/class\/sound\/card0\/hdaudioC0D0\/reconfig/d" ${fileTMP}_rclocal
        sed -i -e "/modprobe asus_nb_wmi/d" ${fileTMP}_rclocal
        sed -i -e "/sys\/class\/leds\/asus::kbd_backlight\/brightness/d" ${fileTMP}_rclocal
        sed -i -e "/sys\/class\/backlight\/intel_backlight\/brightnes/d" ${fileTMP}_rclocal
        # titles
        sed -i -e "/# Asus N46VB woofer Bang & Olufen ICEpower/d" ${fileTMP}_rclocal
        sed -i -e "/# Asus N46VB Keyboard backlightning/d" ${fileTMP}_rclocal
        sed -i -e "/# default Values Display lightning & Keyboard backlightning/d" ${fileTMP}_rclocal
        
        # write values into target
        echo "# Asus N46VB woofer Bang & Olufen ICEpower" >> ${fileTMP}_rclocal 
        echo "echo 0x16 0x99130112 > /sys/class/sound/card0/hdaudioC0D0/user_pin_configs" >> ${fileTMP}_rclocal
        echo "echo 1 > /sys/class/sound/card0/hdaudioC0D0/reconfig" >> ${fileTMP}_rclocal
        
        echo "# Asus N46VB Keyboard backlightning" >> ${fileTMP}_rclocal
        echo "modprobe asus_nb_wmi" >> ${fileTMP}_rclocal
        
        echo "# default Values Display lightning & Keyboard backlightning" >> ${fileTMP}_rclocal
        echo "echo 1   > /sys/class/leds/asus::kbd_backlight/brightness" >> ${fileTMP}_rclocal
        echo "echo 500 > /sys/class/backlight/intel_backlight/brightness" >> ${fileTMP}_rclocal
        
        echo "exit 0" >> ${fileTMP}_rclocal
        
        cat ${fileTMP}_rclocal > ${target} 
        
        echo "RC Local configurado"
        grep -i "0x16 0x99130112" ${target} > /dev/null && echo "Asus N46VB woofer Bang & Olufen ICEpower, ready"
        grep -i "1   > /sys/class/leds/asus::kbd_backlight/brightness" ${target} > /dev/null && echo "Keyboard Backlight, ready"
        grep -i "500 > /sys/class/backlight/intel_backlight/brightness" ${target} > /dev/null && echo "Display Backlight, ready"
    else
        echo "[] No permissions"
    fi
fi

exit 0
