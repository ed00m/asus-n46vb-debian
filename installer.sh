#  installer.sh
#  
#  Copyright 2015 th3d00m <pabloze@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

#!/bin/sh -x


. ./scripttools/terminal/printColor.sh
. ./scripttools/users/users.sh
. ./scripttools/fs/dir.sh
. ./scripttools/users/sudoers.sh

execUser=$(whoami)

if [ "${execUser}" = "root" ];then


    pinfo "Please enter your username"
    read user

    u_checkUser --user ${user} --mode im        # -> userPath, userSHELL and userName||user
    checkDir --path ${userPath} --mode im       # If exists, i can write into home files

    if [ "${userSHELL}" = "/bin/zsh" ];then
        SHELLsource=${userPath}/.zshrc
    elif [ "${userSHELL}" = "/bin/bash" ];then
        SHELLsource=${userPath}/.bashrc
    fi

    mkdir -p ${userPath}/.Apps/

    cp -f displayBacklight.sh   ${userPath}/.Apps/
    cp -f keyboardBacklight.sh  ${userPath}/.Apps/

    createPermissionsUser --user  ${user} --command ${userPath}/.Apps/displayBacklight.sh --type full
    createPermissionsUser --user  ${user} --command ${userPath}/.Apps/keyboardBacklight.sh --type full

    echo "alias dbl=\"sudo ${userPath}/.Apps/displayBacklight.sh\""     >> ${SHELLsource}
    echo "alias kbl=\"sudo ${userPath}/.Apps/keyboardBacklight.sh\""    >> ${SHELLsource}
else
    perror "Not Admin permissions, Try with user root"
fi

exit 0
