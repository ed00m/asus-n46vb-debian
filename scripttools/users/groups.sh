#  groups.sh
#  
#  Copyright 2015 th3d00m <pabloze@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

#  
#  name: asignNewLocalGroup
#  @param Name of NewGroup and username
#  @@print list user details
#  
g_asignNewLocalGroup()
{
    newGroup=$1
    User=$2
    
    gpasswd -a ${User} ${newGroup}
    grep -En "${User}|${newGroup}" /etc/passwd
}

#  
#  name: create
#  @param groupName
#  @@print groupName validation
#  
g_createGroup()
{
    group=$1
    addgrp ${group}
}
#  
#  name: delete
#  @param groupName
#  @@print groupName validation
#  
g_deleteGroup()
{
    group=$1
    delgroup ${group}
}
