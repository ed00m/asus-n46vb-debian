#  sudoers.sh
#  
#  Copyright 2015 th3d00m <pabloze@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

#  
#  name: createPermissionsUser
#  @param Username, type (full or empty), command (full path of command or empty)
#  @@print validation
#  
createPermissionsUser()
{
    User=$1
    type=$2
    command=$3
    
    if [ "${type}" = "full" ] && [ ! -z "$command" ];then
        echo "${User} ALL=NOPASSWD: ${command}" >> /etc/sudoers
    elif [ "${type}" = "full" ] && [ -z "$command" ];then
        echo "${User} ALL=NOPASSWD: ALL" >> /etc/sudoers
    else
        echo "${User} ALL=(ALL:ALL) ALL" >> /etc/sudoers
    fi
}

#  
#  name: createPermissionsGroup
#  @param Groupname, type (full or empty), command (full path of command or empty)
#  @@print validation
#  
createPermissionsGroup()
{
    Group=$1
    type=$2
    command=$3
    
    if [ "${type}" = "full" ] && [ ! -z "$command" ];then
        echo "%${Group} ALL=NOPASSWD: ${command}" >> /etc/sudoers
    elif [ "${type}" = "full" ] && [ -z "$command" ];then
        echo "%${Group} ALL=NOPASSWD: ALL" >> /etc/sudoers
    else
        echo "%${Group} ALL=(ALL:ALL) ALL" >> /etc/sudoers
    fi
}
