#  users.sh
#  
#  Copyright 2015 th3d00m <pabloze@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

# Examples
# u_checkUser   --user test2 || u_checkUser   --user test2 --mode im
# u_createUser  --user test2 || u_createUser  --user test2 --key test1234
# u_deleteUser  --user test2 || u_deleteUser  --user test2 --deep full

#  
#  name: create
#  @param userName
#  @@print userName validation
#  
u_createUser()
{
    # Get options
    for o; do
        case "${o}" in
            --user)         shift; user="${1}"; shift; ;;
            --key)          shift; key="${1}"; shift; ;;
        esac
    done
    
    if [ -z ${key} ];then
        key=$(tr -dc A-Za-z0-9_ < /dev/urandom | head -c 20)
    fi
    # Library missing: aptitude install libcrypt-unixcrypt-perl
    crypt=$(perl -e"  use Crypt::PasswdMD5; print unix_md5_crypt(${key});")
    
    if (useradd ${user} -m -p ${crypt} -d /home/${user});then
        echo "user: "${user}
        echo "key: "${key}
    else
        u_checkUser --user ${user}
    fi
}
#  
#  name: delete
#  @param userName || optional "full" remove home
#  @@print userName validation
#  
u_deleteUser()
{
    # Get options
    for o; do
        case "${o}" in
            --user)         shift; user="${1}"; shift; ;;
            --deep)         shift; deep="${1}"; shift; ;; # im=interactive mode; ni=noninteractive
        esac
    done
    
    if [ "${deep}" = "full" ];then
        if (deluser --remove-home ${user});then
            echo "${user} deleted"
            return 0
        else
            u_checkUser --user ${user}
        fi
    elif [ "${deep}" = "passive" ] || [ -z ${deep} ];then
        if (deluser ${user});then
            echo "${user} deleted"
            return 0
        else
            u_checkUser --user ${user}
        fi
    fi
}
#  
#  name: u_checkUser
#  @param username
#  @return 0 if exists
#  
u_checkUser()
{
    # Get options
    for o; do
        case "${o}" in
            --user)         shift; user="${1}"; shift; ;;
            --mode)         shift; mode="${1}"; shift; ;; # im=interactive mode; ni=noninteractive
        esac
    done
    
    if (grep "${user}:" /etc/passwd > /dev/null);then
        echo "User ${user} found, loading info"
        userPath=$(grep "${user}:" /etc/passwd|awk -F: '{ print $6 }')
        userName=$(grep "${user}:" /etc/passwd|awk -F: '{ print $1 }')
        userSHELL=$(grep "${user}:" /etc/passwd|awk -F: '{ print $7 }')
        
        return 0
    else
        if [ "${mode}" = "im" ];then
            echo "Username not exists, This sure your user is ${user}??, Enter your username now"
            read user
            u_checkUser --user ${user} --mode ${mode}
        elif [ "${mode}" = "ni" ] || [ -z ${mode} ];then
            echo "User ${user} not exists"
        else
            echo "None option"
        fi
    fi
}
