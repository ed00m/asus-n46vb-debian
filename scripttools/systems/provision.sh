#  provision.sh
#  
#  Copyright 2015 th3d00m <pabloze@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

#  
#  name: provision
#  @param none
#  @@print full info from operating systems
#  
provision()
{
    echo "Operating System"
    echo "\t[] detecting support to OS"
    uname -a|grep -E "i386|i486|i586|i686" && export SO="32bits" || export SO="64bits"
    prov_version=$(cat /etc/debian_version)
    proc_version=$(cat /proc/version)
        
    echo "\t[] OS ${SO}"
    echo "\t[] Manufacturer Version: ${prov_version}"
    echo "\t[] Manufacturer Version Kernel: ${proc_version}"
        
    echo "Hardware"
        
    kernel=$(uname -s -r -v)
    maquina=$(uname -n -m -o)
    memoria=$(free -m|awk '/total/ || /Mem/ || /Swap/ { print $1"\t"$2 }'|sed -e "s@total@\ttotal@" -e "s@used@@" -e "s@Mem@\t[] Mem\t@" -e "s@Swap@\t[] Swap\t@")
    cpu_number=$(grep processor /proc/cpuinfo|wc -l)
    cpu_model=$(grep "model name" /proc/cpuinfo|head -1)
    cpu_cache=$(grep "cache size" /proc/cpuinfo|head -1)
    grep -q "^flags.*\blm\b" /proc/cpuinfo && export CPU="64bits" || export CPU="32bits"

    echo "\t[] name: ${maquina}"
    echo "\t[] kernel-version: ${kernel}"
    echo "\t[] Memory: ${memoria}"
    echo "[--] Detecting support CPU"
    echo "\t[] CPU ${CPU}"
    echo "\t[] CPU : ${cpu_number} processors number"
    echo "\t[] CPU : ${cpu_model}"
    echo "\t[] CPU : ${cpu_cache}"
}
