#  devicesNet.sh
#  
#  Copyright 2015 th3d00m <pabloze@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

#  
#  name: devicesNetAll
#  @param none
#  @@print Name|MAC|IP
#  
devicesNetAll()
{
    DEVICES=$(ls -1 /proc/net/dev_snmp6)

    for INTERFACES in ${DEVICES};do
        DEVNAME=${INTERFACES}
        DEVMAC=$(ifconfig ${INTERFACES} |awk '/HWaddr/ { print $5 }')
        DEVIP=$(ifconfig ${INTERFACES} | awk '/inet addr/ {split ($2,A,":"); print A[2]}')

        echo ${DEVNAME}"|"${DEVMAC}"|"${DEVIP}
    done
}
