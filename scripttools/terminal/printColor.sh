#  printColor.sh
#  
#  Copyright 2015 th3d00m <pabloze@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#

#  
#  name: perror
#  @param  | ${message} | text var
#  @@print ${message} in red color
#  
perror()
{
    message=$1
    printf '|| ''\033[0;31m%s\033[0m\n' "${message}"
}

#  
#  name: pok
#  @param | ${message} | text var
#  @@print ${message} in green color
#  
pok()
{
    message=$1
    printf '|| ''\033[0;32m%s\033[0m\n' "${message}"
}

#  
#  name: pinfo
#  @param | ${message} | text var
#  @@print ${message} in white color
#  
pinfo()
{
    message=$1
    printf '|||| ''\033[0;37m%s\033[0m\n' "${message}"
}

#  
#  name: pwarn
#  @param | ${message} | text var
#  @@print ${message} in yellow color
#  
pwarn()
{
    message=$1
    printf '|| ''\033[0;33m%s\033[0m\n' "${message}"
}

#  
#  name: pTitle
#  @param | ${message} | text var
#  @@print ${message} in blue color
#  
pTitle()
{
    message=$1
    printf '|| ''\033[1;34m%s\033[0m\n' "${message}"
}
